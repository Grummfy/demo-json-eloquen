<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Demo extends Migration
{
    public function up()
    {
	    Schema::create('contacts', function(Blueprint $table)
	    {
		    $table->increments('id');
		    $table->timestamps();
		    $table->enum('type', ['billing', 'support']);
		    $table->json('phones')->nullable();
		    $table->json('emails')->nullable();
		    $table->json('address');
	    });

	    Schema::create('contact_phones', function(Blueprint $table)
	    {
		    $table->increments('id');
		    $table->unsignedInteger('contact_id');
		    $table->string('prefix', 4);
		    $table->string('number', 16);
		    $table->string('msisdn', 20);

		    $table->foreign('contact_id')
			    ->references('id')
			    ->on('contacts');
	    });

	    Schema::create('contact_emails', function(Blueprint $table)
	    {
		    $table->increments('id');
		    $table->unsignedInteger('contact_id');
		    $table->string('email', 255);

		    $table->foreign('contact_id')
			    ->references('id')
			    ->on('contacts');
	    });

	    Schema::create('shops', function(Blueprint $table)
	    {
		    $table->increments('id');
		    $table->timestamps();
		    $table->string('name');
		    $table->json('address');
		    $table->string('street1')->virtualAs('address->>\'$.street\'');
		    $table->string('street2')->storedAs('address->>\'$.street\'');
	    });
    }

    public function down()
    {
	    Schema::dropIfExists('contact_emails');
	    Schema::dropIfExists('contact_phones');
	    Schema::dropIfExists('shops');
	    Schema::dropIfExists('contacts');
    }
}
