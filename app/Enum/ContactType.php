<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self BILLING()
 * @method static self SUPPORT()
 */
class ContactType extends Enum
{
	const BILLING = 'billing';
	const SUPPORT = 'support';
}
