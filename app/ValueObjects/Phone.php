<?php

namespace App\ValueObjects;

use Grummfy\EloquentExtendedCast\Contracts\StdAbleData;
use Illuminate\Contracts\Support\Arrayable;

class Phone implements Arrayable, StdAbleData
{
	/**
	 * @var string
	 */
	protected $number;

	/**
	 * @var string
	 */
	protected $prefix;

	/**
	 * @var string
	 */
	protected $msisdn;

	public static function fromStdClass(\stdClass $data)
	{
		return new static($data->prefix, $data->number, $data->msisdn ?? null);
	}

	public function __construct(string $prefix, string $number, string $msisdn = null)
	{
		$this->number = preg_replace('/[^0-9]/', '', $number);
		$this->prefix = preg_replace('/[^0-9]/', '', $prefix);
		$this->msisdn = $msisdn ?? $this->computeMsisdn();
	}

	public function computeMsisdn()
	{
		return ltrim($this->getPrefix(), '0') . ltrim($this->getNumber(), '0');
	}

	public function getNumber(): string
	{
		return $this->number;
	}

	public function getPrefix(): string
	{
		return $this->prefix;
	}

	public function getMsisdn(): string
	{
		return $this->msisdn;
	}

	public function __toString(): string
	{
		return '(+' . $this->getPrefix() . ') ' . $this->getNumber();
	}

	public function toArray(): array
	{
		return [
			'prefix' => $this->getPrefix(),
			'number' => $this->getNumber(),
			'msisdn' => $this->getMsisdn(),
		];
	}
}
