<?php

namespace App\ValueObjects;

use Grummfy\EloquentExtendedCast\Contracts\StdAbleData;

class Address implements StdAbleData, \JsonSerializable
{
	/**
	 * @var string
	 */
	protected $number;

	/**
	 * @var string|null
	 */
	protected $box;

	/**
	 * @var string
	 */
	protected $street;

	/**
	 * @var string
	 */
	protected $zip;

	/**
	 * @var string
	 */
	protected $city;

	/**
	 * @var string
	 */
	protected $country;

	/**
	 * @var float|null
	 */
	protected $latitude;

	/**
	 * @var float|null
	 */
	protected $longitude;

	/**
	 * Build a new object instance from the stdClass data
	 *
	 * @return self
	 */
	public static function fromStdClass(\stdClass $data)
	{
		return new static(
			$data->number,
			$data->box,
			$data->street,
			$data->zip,
			$data->city,
			$data->country,
			$data->coordinates->latitude,
			$data->coordinates->longitude
		);
	}

	public function __construct(string $number, ?string $box, string $street, string $zip, string $city, string $country, ?float $latitude, ?float $longitude)
	{
		$this->number = $number;
		$this->box = $box;
		$this->street = $street;
		$this->zip = $zip;
		$this->city = $city;
		$this->country = $country;
		$this->latitude = $latitude;
		$this->longitude = $longitude;
	}

	public function getNumber(): string
	{
		return $this->number;
	}

	public function getBox(): ?string
	{
		return $this->box;
	}

	public function getStreet(): string
	{
		return $this->street;
	}

	public function getZip(): string
	{
		return $this->zip;
	}

	public function getCity(): string
	{
		return $this->city;
	}

	public function getCountry(): string
	{
		return $this->country;
	}

	public function getLatitude(): ?float
	{
		return $this->latitude;
	}

	public function getLongitude(): ?float
	{
		return $this->longitude;
	}

		// mandatory to be converted to json by laravel, one of:
	// * Illuminate\Contracts\Support\Arrayable
	// * Illuminate\Contracts\Support\Jsonable
	// * \JsonSerializable
	// that's for collection, but a single element => \JsonSerializable for the json_encode of php itself
	public function jsonSerialize()
	{
		return [
			'street' => $this->getStreet(),
			'number' => $this->getNumber(),
			'box' => $this->getBox(),
			'zip' => $this->getZip(),
			'city' => $this->getCity(),
			'country' => $this->getCountry(),
			'coordinates' => [
				'latitude' => $this->getLatitude(),
				'longitude' => $this->getLongitude(),
			],
		];
	}

	public function __toString()
	{
		return $this->getStreet() . ', ' . $this->getNumber() . PHP_EOL
		       . $this->getZip() . ' ' . $this->getCity() . PHP_EOL
		       . $this->getCountry();
	}
}
