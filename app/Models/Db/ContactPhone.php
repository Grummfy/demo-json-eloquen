<?php

namespace App\Models\Db;

use App\Models\Eloquent as Eloquent;
use App\ValueObjects\Phone;
use Grummfy\EloquentExtendedCast\Eloquent\CastableModel;

/**
 * Class ContactPhone
 * 
 * @property int $id
 * @property int $contact_id
 * @property string $prefix
 * @property string $number
 * @property string $msisdn
 *
 * @property Phone $phone
 * 
 * @property \App\Models\Db\Contact $contact
 *
 * @mixin Eloquent
 */
class ContactPhone extends Eloquent
{
	// use advanced cast
	use CastableModel;

	public $timestamps = false;

	protected $casts = [
		'contact_id' => 'int',
		'phone' => 'jsonCollection',
	];

	protected $fillable = [
		'contact_id',
		'prefix',
		'number',
		'msisdn',
	];

	public function contact()
	{
		return $this->belongsTo(Contact::class);
	}

	public function setPhoneAttribute(Phone $phone): self
	{
		$this->prefix = $phone->getPrefix();
		$this->number = $phone->getNumber();
		$this->msisdn = $phone->getMsisdn();
		return $this;
	}

	public function getPhoneAttribute(): Phone
	{
		return new Phone($this->prefix, $this->number, $this->msisdn);
	}
}
