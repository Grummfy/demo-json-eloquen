<?php

namespace App\Models\Db;

use App\Models\Eloquent as Eloquent;

/**
 * @property int $id
 * @property int $contact_id
 * @property string $email
 * 
 * @property \App\Models\Db\Contact $contact
 *
 * @mixin Eloquent
 */
class ContactEmail extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'contact_id' => 'int',
	];

	protected $fillable = [
		'contact_id',
		'email',
	];

	public function contact()
	{
		return $this->belongsTo(Contact::class);
	}
}
