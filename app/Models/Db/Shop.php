<?php

namespace App\Models\Db;

use App\Models\Eloquent as Eloquent;
use App\ValueObjects\Address;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $name
 * @property Address $address
 *
 * @mixin \Eloquent
 */
class Shop extends Eloquent
{
	protected $fillable = [
		'name',
		'address',
	];

	public function getAddressAttribute(string $value): Address
	{
		return Address::fromStdClass(json_decode($value));
	}

	public function setAddressAttribute(Address $address)
	{
		$this->attributes['address'] = json_encode($address);
	}
}
