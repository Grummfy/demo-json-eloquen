<?php

namespace App\Models\Db;

use App\Models\Eloquent as Eloquent;
use App\ValueObjects\Address;
use App\ValueObjects\Phone;
use Grummfy\EloquentExtendedCast\Eloquent\CastableModel;
use Grummfy\EloquentExtendedCast\Eloquent\JsonReadOnlyCollectionCastable;
use Grummfy\EloquentExtendedCast\ValueObject\ReadOnlyCollection;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $type
 * @property \Grummfy\EloquentExtendedCast\ValueObject\ReadOnlyCollection|\App\ValueObjects\Phone[] $phones
 * @property string[] $emails
 * @property Address $address
 * 
 * @property \Illuminate\Database\Eloquent\Collection $contact_emails
 * @property \Illuminate\Database\Eloquent\Collection $contact_phones
 *
 * @mixin Eloquent
 */
class Contact extends Eloquent
{
	// use advanced cast
	use CastableModel;
	// collection of json cast
	use JsonReadOnlyCollectionCastable;

	protected $casts = [
		'emails' => 'array', // or json

		// custom types
		'phones' => 'jsonReadOnlyCollection',
		'address' => 'address',
	];

	protected $fillable = [
		'type',
		'phones',
		'emails',
		'address',
	];

	protected $castParameters = [
		'phones' => Phone::class,
	];

	public function contact_emails()
	{
		return $this->hasMany(ContactEmail::class);
	}

	public function contact_phones()
	{
		return $this->hasMany(ContactPhone::class);
	}

	public function addPhone(string $prefix, string $number): self
	{
		if (is_null($this->phones))
		{
			$this->phones = new ReadOnlyCollection();
		}
		$this->phones = $this->phones->push(new Phone($prefix, $number));
		return $this;
	}

	//
	// casting methods
	//

	public function toAddress(Address $address): string
	{
		return json_encode($address);
	}

	public function fromAddress(string $address): Address
	{
		return Address::fromStdClass($this->fromJson($address, true));
	}
}
