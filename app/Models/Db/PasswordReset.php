<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 13 Feb 2018 22:49:29 +0000.
 */

namespace App\Models\Db;

use App\Models\Eloquent as Eloquent;

/**
 * Class PasswordReset
 * 
 * @property string $email
 * @property string $token
 * @property \Carbon\Carbon $created_at
 *
 * @package App\Models\Db
 */
class PasswordReset extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'email',
		'token'
	];
}
