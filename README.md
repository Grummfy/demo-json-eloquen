# Demo json eloqient

## Install

To install we run a docker-compose and we will connect to it to install the dependencies

	cp .env.example .env
	cd externals/docker
	docker-compose -p demojson up

	composer install
	docker exec -ti demojson_web_1
	php artisan key:generate
	chmod -R auo+w storage/
	exit

## Run the docker

	docker-compose -p demojson up

## Dev

### Frontend assets

If you have made change, you can refresh the design with theses commands, otherwise it's useless.

See https://laravel.com/docs/5.6/frontend & https://laravel.com/docs/5.6/mix

If you have node installed

```
npm install
npm run dev
```

with docker

```
docker run -it --rm -v ${PWD}:/toto -w /toto node npm install
docker run -it --rm -v ${PWD}:/toto -w /toto node npm run dev
```

### Generate ide helper

When you install new dependencies, you perhaps need to run these commands.

From the docker:

```
php artisan ide-helper:generate
php artisan ide-helper:meta
```

### Generate new model from database

If you add a new model in the database

```
php artisan code:models --table=NewTable
```
