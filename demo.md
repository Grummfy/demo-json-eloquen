# Demo

## Models create/read

* run docker-compose
* log inside the web docker in bash
* refresh the data `a migrate:fresh`
* go to tinker with `a t`
* run the following expressions

### Basic record json using attributes

	$a = new Address(42, null, 'teststraat', 1234, 'Test', 'Belgium', null, null)
	$shop = App\Models\Db\Shop::create(['address' => $a, 'name' => 'a shop'])
	$shop->address
	$shop
	Shop::first();

### Basic record json & casting

	$a = new Address(42, null, 'teststraat', 1234, 'Test', 'Belgium', null, null)
	$contact = App\Models\Db\Contact::create(['address' => $a, 'emails' => ['toto@example.tld', 'me@you.world']])
	$contact->address
	$contact
	Contact::first();

### Collection

Traditional ways
	
	$a = new Address(42, null, 'teststraat', 1234, 'Test', 'Belgium', null, null)
	$contact = Contact::create(['address' => $a])
	$contactPhone = ContactPhone::make()
	$contactPhone->phone = new Phone(123, 456789)
	$contactPhone
	$contact->contact_phones()->save($contactPhone)
	$contact->contact_phones
	Contact::with('contact_phones')->get()

Value object and json way (one of)

	$a = new Address(42, null, 'teststraat', 1234, 'Test', 'Belgium', null, null)
	$a
	$b = Contact::make(['address' => $a, 'type' => 'billing'])
	$b->phones
	$b->addPhone(12, 34567);
	$b->phones->toCollection()->each(function($i){echo  $i;})
	$b->phones
	$b
	$b->save();
	Contact::all();
	Contact::with('contact_phones')->get()

### Search

Naive way

	$emailSearch = 'me@you.world';
	Contact::where('email', 'like', '%' . $emailSearch . '%')->get()
	$citySearch = 'Test'
	Contact::where('address', 'like', '%' . $citySearch . '%')->get()

Correct way

	Contact::where('address->city', $citySearch)->get()
	Contact::whereRaw('JSON_CONTAINS_PATH(address, ?, ?, ?)', ['one', '$.city', '$.coordinates.latitude'])->get()
	Contact::whereRaw('JSON_CONTAINS_PATH(address, ?, ?, ?)', ['all', '$.city', '$.coordinates.latitude'])->get()
	
	
	Contact::whereRaw('JSON_CONTAINS(phones, ?)', json_encode(['prefix' => 12]))->get()
	Contact::whereRaw('JSON_CONTAINS(phones, ?)', json_encode(['prefix' => '12']))->get()
	// SELECT *, phones->'$[*].prefix' FROM `contacts` WHERE JSON_CONTAINS(phones, '{"prefix": "12"}')
	// SELECT *, phones->>'$[*].prefix' FROM `contacts` WHERE JSON_CONTAINS(phones, '{"prefix": "12"}')
	// /!\ col phones in result are not filtered ;)
	
	Contact::where('phones->$[0].prefix', 12)->get()
	
see https://dev.mysql.com/doc/refman/5.7/en/json-search-functions.html
see https://dev.mysql.com/doc/refman/5.7/en/json-path-syntax.html
